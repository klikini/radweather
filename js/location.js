let latitude = null;
let longitude = null;

function getLocation(callback) {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function (position) {
			latitude = position.coords.latitude;
			longitude = position.coords.longitude;
			callback();
		}, function () {
			alert('Geolocation is required to access weather data.');
		});
	} else {
		alert('Geolocation is not supported by this browser.');
	}
}

function update() {
	if (longitude == null || latitude == null) {
		getLocation(function () {
			getWeather();
		});
	} else {
		getWeather();
	}
}

$(document).ready(function () {
	update();
});
