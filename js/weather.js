// Units

$(document).ready(function () {
	if (window.localStorage['units']) {
		$('input[name="units"][value="' + window.localStorage['units'] + '"]').prop('checked', true);
	}

	$('input[name="units"]').change(function () {
		if (this.checked) {
			window.localStorage['units'] = this.value;
			update();
		}
	});
});

function getUnits() {
	return $('input[name="units"]:checked').val();
}

function getTempSymbol() {
	switch (getUnits()) {
		case 'us':
			return 'F';
		case 'si':
		default:
			return 'C';
	}
}

// Language

function getLanguage() {
	return $('html').attr('lang');
}

// Weather

function getWeather() {
	let url = "weather.php"
		+ "?latitude=" + latitude
		+ "&longitude=" + longitude
		+ "&units=" + getUnits()
		+ "&language=" + getLanguage();

	$.getJSON(url, function (data) {
		if (data['error']) {
			alert('Error getting weather data: ' + data['error']);
		} else {
			displayWeather(data);
		}
	});
}

// Radians

function degToRad(temp) {
	let numerator = temp; // will include pi
	let denominator = 180;

	// Round to integer
	numerator = Math.round(numerator);

	// Simplify
	for (let i = 10; i > 0; i--) {
		if (numerator % i === 0 && denominator % i === 0) {
			numerator /= i;
			denominator /= i;
		}
	}

	// Remove redundant 1 coefficient
	if (numerator === 1) {
		numerator = '';
	} else if (numerator === -1) {
		numerator = '-';
	}

	let rad = numerator;

	// Value is just zero
	if (numerator === 0) {
		return rad;
	}

	rad += pi;

	// Add denominator if not redundant
	if (denominator > 1) {
		rad += '/' + denominator;
	}

	return rad;
}
