let pi = '\u03C0';

function displayWeather(data) {
	// Remove existing display
	$("#weather .row").empty();

	// Make new displays
	$("#weather #currently").append(getCard(data['currently'], 'current'));

	$.each(data['hourly']['data'], function(i, hour) {
		$("#weather #hourly").append(getCard(hour, 'hour'));
	});

	$.each(data['daily']['data'], function(i, day) {
		$("#weather #daily").append(getCard(day, 'day'));
	});

	// Render fractions
	$('.fraction').each(function() {
		let split = $(this).html().split("/");

		if (split.length === 2) {
			$(this).html(
				'<span class="numerator">' + split[0] + '</span>' +
				'<span class="invisible">/</span>' +
				'<span class="denominator">' + split[1] + '</span>'
			);
		}
	});

	function getCardTitle(data, type) {
		let date = new Date(data['time'] * 1000);

		switch (type) {
			case 'hour':
				let hours = date.getHours();

				if (hours > 12) {
					hours -= 12;
					hours += ' PM';
				} else {
					if (hours === 0) {
						hours = 12;
					}

					hours += ' AM';
				}

				return hours + ' on ' + getCardTitle(data, 'day');
			case 'day':
				return (date.getMonth() + 1) + '/' + date.getDate();
		}
	}

	function formatTemp(temp) {
		return '<span class="fraction">' + degToRad(temp) + '</span>'
			+ ' ' + getTempSymbol();
	}

	function getCard(data, type) {
		let card = $('<fieldset/>', {
			class: 'card'
		});

		if (type !== 'current') {
			$('<legend/>', {
				text: getCardTitle(data, type)
			}).appendTo(card);
		}

		$('<div/>', {
			class: 'summary',
			html: '<p>' + data['summary'] + '</p>'
		}).appendTo(card);

		$('<div/>', {
			class: 'precipProbability',
			html: '<p>' + Math.round(data['precipProbability'] * 100) + '% chance of precip.</p>'
		}).appendTo(card);

		if (type === 'day') {
			$('<div/>', {
				class: 'temperature',
				html: '<p><b>Low Temp:</b> ' + formatTemp(data['temperatureLow']) + '</p>'
			}).appendTo(card);

			$('<div/>', {
				class: 'temperature',
				html: '<p><b>High Temp:</b> ' + formatTemp(data['temperatureHigh']) + '</p>'
			}).appendTo(card);
		} else {
			$('<div/>', {
				class: 'temperature',
				html: '<p><b>Temp:</b> ' + formatTemp(data['temperature']) + '</p>'
			}).appendTo(card);
		}

		$('<div/>', {
			class: 'humidity',
			html: '<p><b>Humidity:</b> ' + Math.round(data['humidity'] * 100) + '%</p>'
		}).appendTo(card);

		return card;
	}
}

// Page theme

function getBackgroundColor() {
	if (window.localStorage['bgcolor']) {
		return window.localStorage['bgcolor'];
	} else {
		return '#eeeeee';
	}
}

function setBackgroundColor(color, fromStorage) {
	window.localStorage['bgcolor'] = color;
	$('body').css('background-color', color);

	if (fromStorage) {
		$('input[name="bgcolor"]').val(color);
	}
}

function getTextColor() {
	if (window.localStorage['textcolor']) {
		return window.localStorage['textcolor'];
	} else {
		return '#000000';
	}
}

function setTextColor(color, fromStorage) {
	window.localStorage['textcolor'] = color;
	$('body').css('color', color);
	$('.denominator').css('border-color', color);

	if (fromStorage) {
		$('input[name="textcolor"]').val(color);
	}
}

$(document).ready(function () {
	$('input[name="bgcolor"]').change(function() {
		setBackgroundColor(this.value, false);
	});

	$('input[name="textcolor"]').change(function() {
		setTextColor(this.value, false);
	});

	setBackgroundColor(getBackgroundColor(), true);
	setTextColor(getTextColor(), true);
});
