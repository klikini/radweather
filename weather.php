<?php

require_once "config.php";

header("Content-Type: application/json");

// Check referrer
$domain = substr($_SERVER["HTTP_REFERER"], strpos($_SERVER["HTTP_REFERER"], "://") + 3);

foreach (["/", ":"] as $char) {
	if (strpos($domain, $char) !== false) {
		$domain = substr($domain, 0, strpos($domain, $char));
	}
}

if ($domain != host_domain) {
	exit(json_encode([
		"error" => "Missing or invalid referrer"
	]));
}

// Check parameters
foreach (["latitude", "longitude"] as $param) {
	if (!isset($_GET[$param]) || !is_numeric($_GET[$param])) {
		exit(json_encode([
			"error" => "Missing or invalid value for parameter $param"
		]));
	}
}

// Get weather data
$url = "https://api.darksky.net/forecast"
	. "/" . dark_sky_secret_key
	. "/" . $_GET["latitude"]
	. "," . $_GET["longitude"]
	. "?units=" . $_GET["units"]
	. "&lang=" . $_GET["language"];
$json = file_get_contents($url);

// Check weather data
if ($json === false) {
	// Return weather data
	exit(json_encode([
		"error" => "Dark Sky API error"
	]));
} else {
	exit($json);
}
