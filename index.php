<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>RadWeather</title>
		<link rel="stylesheet" href="css/page.css">
		<link rel="stylesheet" href="css/weather.css">
	</head>
	<body>
		<div id="weather">
			<h2>Current Conditions</h2>
			<div id="currently" class="row">
			</div>

			<h2>Daily Forecast</h2>
			<div id="daily" class="row">
			</div>

			<h2>Hourly Forecast</h2>
			<div id="hourly" class="row">
			</div>
		</div>

		<h2>Options</h2>
		<div class="row">
			<fieldset>
				<legend>Units</legend>
				<label>
					<input type="radio" name="units" value="us" checked>
					US (Radians F)
				</label>
				<label>
					<input type="radio" name="units" value="si">
					SI (Radians C)
				</label>
			</fieldset>
			<fieldset>
				<legend>Theme</legend>
				<label>
					Background:
					<input type="color" name="bgcolor">
				</label>
				<label>
					Text:
					<input type="color" name="textcolor">
				</label>
			</fieldset>
		</div>

		<!-- Attribution -->
		<hr>
		<a href="https://darksky.net/poweredby/">Powered by Dark Sky</a> |
		<a href="https://gitlab.com/klikini/radweather">Open Source</a>

		<!-- JS -->
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"
				integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
				crossorigin="anonymous"></script>
		<script src="js/display.js"></script>
		<script src="js/location.js"></script>
		<script src="js/weather.js"></script>
	</body>
</html>
